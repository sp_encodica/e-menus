//
//  ShopMediaDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopMedia : Mappable{
    
    var logo: WPImage?
    var image_gallery: [WPImage]?
    
    // Mappable
    func mapping(map: Map) {
        logo <- map["logo"]
        image_gallery  <- map["image_gallery"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
        image_gallery = []
    }
    
}
