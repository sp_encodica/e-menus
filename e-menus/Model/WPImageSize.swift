//
//  WPImageSize.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 05/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class WPImageSize: Mappable{
    // init(){}
    
    required init?(map: Map) {
        
        if map.JSON["thumbnail"] == nil {
            print("--------- error in json  thumbnail ----------")
            return nil
        }
    }
    
    var thumbnail: String? // "https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait-150x150.jpg",
    var thumbnail_width: Int? //150,
    var thumbnail_height: Int? //150,
    var medium: String? //"https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait-179x300.jpg",
    var medium_width: Int? //179,
    var medium_height: Int? //300,
    var medium_large: String? //"https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait.jpg",
    var medium_large_width: Int? //656,
    var medium_large_height: Int? //1100,
    var large: String? //"https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait-611x1024.jpg",
    var large_width: Int? //611,
    var large_height: Int? //1024,
    var custom_0360: String?//:"https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait-360x604.jpg",
    var custom_0360_width: Int? //360,
    var custom_0360_height: Int? //604,
    var custom_0640: String? //"https:\/\/e-menus.optimedia.gr\/wp-content\/uploads\/sponsor_heineken_portrait-640x1073.jpg",
    var custom_0640_width: String? //640,
    var custom_0640_height: Int? //1073
    
    // Mappable
    func mapping(map: Map) {
        thumbnail <- map["thumbnail"]
        thumbnail_width <- map["thumbnail_width"]
        thumbnail_height <- map["thumbnail_height"]
        medium <- map["medium"]
        medium_width <- map["medium_width"]
        medium_height <- map["medium_height"]
        medium_large <- map["medium_large"]
        medium_large_width <- map["medium_large_width"]
        medium_large_height <- map["medium_large_height"]
        large  <- map["large"]
        large_width <- map["large_width"]
        large_height <- map["large_height"]
        custom_0360  <- map["custom_0360"]
        custom_0360_width <- map["custom_0360_width"]
        custom_0360_height <- map["custom_0360_height"]
        custom_0640 <- map["custom_0640"]
        custom_0640_width <- map["custom_0640_width"]
        custom_0640_height <- map["custom_0640_height"]
        
    }
}
