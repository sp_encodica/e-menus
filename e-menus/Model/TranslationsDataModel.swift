//
//  TranslationsDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 18/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class Translation: Mappable {
    
    var post_id: Int?
    var lang_code: String?
    var source_id: Int?
    var name: String?
    var description: String?
    var logo: String?
    
    // Mappable
    func mapping(map: Map) {
        post_id <- map["post_id"]
        lang_code  <- map["lang_code"]
        source_id <- map["source_id"]
        name <- map["name"]
        description  <- map["description"]
        logo <- map["logo"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
}
