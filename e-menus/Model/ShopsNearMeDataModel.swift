//
//  ShopsNearMe.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 18/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopsNearMeDataModel: Mappable{
    
    var distance:Int
    var radius: Int
    var translations: [Translation]?
    
    // Mappable
    func mapping(map: Map) {
        distance <- map["distance"]
        radius  <- map["radius"]
        translations <- map["translations"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
        distance = 0
        radius = 0
    }
    
}
