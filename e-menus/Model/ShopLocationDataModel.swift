//
//  ShopLocationDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopLocation: Mappable {
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
    var map_point: MapPoint?
    var radius: String?
    
    // Mappable
    func mapping(map: Map) {
        map_point <- map["map_point"]
        radius  <- map["radius"]
    }
}

