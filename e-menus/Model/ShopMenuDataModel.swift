//
//  ShopMenuDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopMenu: Mappable {
    
    var menu_name: String?
    var menu_type: ShopMenuType?
    var menu_memo: String?
    var menu_order_notes: String?
    var menu_categories: [MenuCategory]?
    
    // Mappable
    func mapping(map: Map) {
        menu_name <- map["menu_name"]
        menu_type  <- map["menu_type"]
        menu_memo <- map["menu_memo"]
        menu_order_notes  <- map["menu_order_notes"]
        menu_categories <- map["menu_categories"]
        
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
}
