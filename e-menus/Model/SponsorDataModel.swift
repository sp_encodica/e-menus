//
//  SponsorDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 21/03/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper



class SponsorDataModel: Mappable{
    
    var id: Int?
    var url: String?
    var weight: Int?
    var img_landscape: WPImage?
    var img_portrait: WPImage?
    
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id      <- map["id"]
        url  <- map["url"]
        img_landscape <- map["img_landscape"]
        img_portrait <- map ["img_portrait"]
    }
    
    /*
    init(){
        id = 0
        weight = 0
        photo_p = ""
        photo_l = ""
        name = ""
    }
 */
}
