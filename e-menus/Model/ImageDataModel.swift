//
//  ImageDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 05/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper


class WPImage: Mappable{
    
    required init?(map: Map) {
        if map.JSON["sizes"] == nil {
            print("--------- error in json sizes ----------")
            return nil
        }
    }
    
    var ID: Int?
    var id: Int?
    var title: String?
    var filename: String?
    var filesize:String?
    var url: String?
    var link: String?
    var alt: String?
    var author: String?
    var description: String?
    var caption: String?
    var name: String?
    var status:String?
    var uploaded_to: Int?
    var date: String? //"2019-03-18 08:25:19",
    var modified: String? //"2019-03-18 08:25:19",
    var menu_order: Int? //0,
    var mime_type: String? //"image\/jpeg",
    var type: String?
    var subtype: String? // "jpeg",
    var icon: String? // "https:\/\/e-menus.optimedia.gr\/wp-includes\/images\/media\/default.png",
    var width: Int? //1100,
    var height: Int? //656,
    var sizes: WPImageSize?
    
    
    // Mappable
    func mapping(map: Map) {
        ID      <- map["ID"]
        id  <- map["id"]
        title <- map["title"]
        filename <- map["filename"]
        filesize <- map["filesize"]
        url <- map["url"]
        link <- map["link"]
        alt <- map["alt"]
        author <- map["author"]
        description <- map["description"]
        caption <- map["caption"]
        name <- map["name"]
        status <- map["status"]
        uploaded_to <- map["uploaded_to"]
        date <- map["date"]
        modified <- map["modified"]
        menu_order <- map["menu_order"]
        mime_type <- map["mime_type"]
        type <- map["type"]
        subtype <- map["subtype"]
        icon <- map["icon"]
        width <- map["width"]
        height <- map["height"]
        sizes <- map["sizes"]
        
    }
    
}
