//
//  ShopCategoryItemDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class CategoryItem: Mappable{
    
    var item_code: String?
    var item_name: String?
    var item_description: String?
    var item_price_1: String?
    var item_price_2: String?
    var item_price_3: String?
    var item_kcals_1: String?
    var item_kcals_2: String?
    var item_kcals_3: String?
    var item_properties: [ItemProperty]?
    
    // Mappable
    func mapping(map: Map) {
        item_code <- map["item_code"]
        item_name  <- map["item_name"]
        item_description <- map["item_description"]
        item_price_1 <- map["item_price_1"]
        item_price_2 <- map["item_price_2"]
        item_price_3 <- map["item_price_3"]
        item_kcals_1 <- map["item_kcals_1"]
        item_kcals_2 <- map["item_kcals_2"]
        item_kcals_3 <- map["item_kcals_3"]
        item_properties <- map["item_properties"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
}
