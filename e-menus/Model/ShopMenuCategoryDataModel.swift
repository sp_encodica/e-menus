//
//  ShopMenuCategoryDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class MenuCategory: Mappable{
    
    var category_name: String?
    var category_description: String?
    var category_price_1: String?
    var category_price_2: String?
    var category_price_3: String?
    var category_items: [CategoryItem]?
    
    // Mappable
    func mapping(map: Map) {
        category_name <- map["category_name"]
        category_description  <- map["category_description"]
        category_price_1 <- map["category_price_1"]
        category_price_2 <- map["category_price_2"]
        category_price_3 <- map["category_price_3"]
        category_items <- map["category_items"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
}
