//
//  ShopDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopDataModel: Mappable{
    
    var lang: String?
    var name: String?
    var description: String?
    var contact_info: ShopContactInfo?
    var location: ShopLocation?
    var media: ShopMedia?
    var banners: String?
    var menus: [ShopMenu]?
   
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
    // Mappable
    func mapping(map: Map) {
        lang <- map["lang"]
        name  <- map["name"]
        description <- map["description"]
        contact_info <- map["contact_info"]
        location <- map["location"]
        media <- map["media"]
        banners <- map["banners"]
        menus <- map["menus"]
    }
    
}
