//
//  ShopContactInfoModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ShopContactInfo: Mappable {
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
    var street: String?
    var postcode_zip: String?
    var city: String?
    var state: String?
    var phone_1: String?
    var phone_2: String?
    var email: String?
    var contact_person:String?

    
    // Mappable
    func mapping(map: Map) {
        street <- map["street"]
        postcode_zip  <- map["postcode_zip"]
        city <- map["city"]
        state <- map["state"]
        phone_1 <- map["phone_1"]
        phone_2 <- map["phone_2"]
        email <- map["email"]
        contact_person <- map["contact_person"]
    }
}
