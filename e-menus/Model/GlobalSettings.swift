//
//  GlobalSettings.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 22/03/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import Alamofire


import SwiftyJSON
import ObjectMapper

// let localStorage = localStorageModel()

class Settings {
    
    static let shared = Settings();
    
    var sponsorsURL : String;
    var shopsAroundMeURL : String;
    var shopDetailsURL : String;
    init(){
        sponsorsURL  = "https://e-menus.optimedia.gr/app/sponsors"
        shopsAroundMeURL = "https://e-menus.optimedia.gr/app/businesses?lat=37.978008&lon=23.707495&rng=2000&max=5"
        shopDetailsURL = "https://e-menus.optimedia.gr/app/business"
    }
    
}

class LocalStorageModel {
    static let sharedInstance =  LocalStorageModel();
    
    var sponsors : [SponsorDataModel];
    var shopsNearMe : [ShopsNearMeDataModel];
    var shopDetails : [ShopDataModel];
    
    init(){
        sponsors = []
        shopsNearMe = []
        shopDetails = []
    }
}

class HelperFunctions {
    
    static let shared = HelperFunctions()
    
    // initializer
    init(){
    }
    func checkConnectivity() -> Bool{
        print("Checking connectivity");
        return true
    }
    
    func checkLocationServices() -> Bool {
        print ("Checking location services");
        return true;
    }
    
    
    /*
    func getUserInfo(completion: @escaping (String) -> Void) {
        Alamofire.request(workingjss.jssURL + devAPIMatchPath + workingData.user, method: .get)
            .authenticate(user: workingjss.jssUsername, password: workingjss.jssPassword).responseString { response in
                if (response.result.isSuccess) {
                    print("In Function Data: \(response.result.value!)"
                        completion(response.result.value!)
                }
        }
    }
    */
    
 // MARK SHOP DETAILS
    
    func getShopDetails(businessId: Int, completion: @escaping (Bool) -> Void) {
        
        Alamofire.request(Settings.shared.shopDetailsURL + "?id=" + String(businessId) ).responseJSON { response in
            if response.result.isSuccess{
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJson);
                self.jsonParseShopDetails(jsonStr: resultJsonStr);
                completion(true)
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
    }
    
    
    
    
    
    
// MARK SHOPS Near ME
    func getShopsNearMe(completion: @escaping (Bool) -> Void) {
        
        // AlamofireService.re
        
        Alamofire.request(Settings.shared.shopsAroundMeURL).responseJSON { response in
            if response.result.isSuccess{
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJson);
                self.jsonParseShopsNearMe(jsonStr: resultJsonStr);
                completion(true)
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
    }
    
// MARK SPONSORS
    func getSponsors(completion: @escaping (Bool) -> Void) {
        
        // AlamofireService.re
        
        Alamofire.request(Settings.shared.sponsorsURL).responseJSON { response in
            if response.result.isSuccess{
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJson);
                self.jsonParseSponsors(jsonStr: resultJsonStr);
                completion(true)
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
    }
    
// PARSING FUNCTIONS
    
    func jsonParseShopDetails(jsonStr: String) -> Void{
        
        // print("json str received" + jsonStr)
        let shopDetails : [ShopDataModel] = Mapper<ShopDataModel>().mapArray(JSONString: jsonStr)!
        
        // assign the read sponsors to the localstorage singleton
        LocalStorageModel.sharedInstance.shopDetails = shopDetails
    }
    
    func jsonParseSponsors(jsonStr: String) -> Void{
        
        // print("json str received" + jsonStr)
        let sponsors : [SponsorDataModel] = Mapper<SponsorDataModel>().mapArray(JSONString: jsonStr)!

        // assign the read sponsors to the localstorage singleton
        LocalStorageModel.sharedInstance.sponsors = sponsors
        
    }

    func jsonParseShopsNearMe(jsonStr: String) -> Void{
        
        // print("json str received" + jsonStr)
        let shopsNearMe : [ShopsNearMeDataModel] = Mapper<ShopsNearMeDataModel>().mapArray(JSONString: jsonStr)!
        
        // assign the read sponsors to the localstorage singleton
        LocalStorageModel.sharedInstance.shopsNearMe = shopsNearMe
        
    }

    
}


// reachability used the Alamo fire
// https://stackoverflow.com/questions/35427698/how-to-use-networkreachabilitymanager-in-alamofire
class NetworkStatus {
    static let sharedInstance = NetworkStatus()
    var _status : NetworkReachabilityManager.NetworkReachabilityStatus = NetworkReachabilityManager.NetworkReachabilityStatus.unknown;
    
    private init() {}
    
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
    
    //NetworkReachabilityManager.NetworkReachabilityStatus
    func startNetworkReachabilityObserver(vc: UIViewController)  {
        reachabilityManager?.listener = { status in
            
            var msg: String = "";
            
            switch status {
                
            case .notReachable:
               msg = "The network is not reachable"
                
            case .unknown :
                msg = "It is unknown whether the network is reachable"
                
            case .reachable(.ethernetOrWiFi):
                msg = "The network is reachable over the WiFi connection"
                
                // self.alert(alertMsg:"The network is reachable over the wifi connection", viewController: vc)
                
            case .reachable(.wwan):
                msg = "The network is reachable over the WWAN connection"
            }
            self._status = status
            self.alert(alertMsg: msg, viewController: vc)
        }
        reachabilityManager?.startListening()
        
    }
    
    func alert(alertMsg: String, viewController: UIViewController) {
        let alert = UIAlertController(title: alertMsg, message: "Please try again later", preferredStyle: .alert)
        
        // alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        viewController.present(alert, animated: true)
    }
}

