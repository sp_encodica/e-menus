//
//  MapPointDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 16/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class MapPoint: Mappable {
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
    var address: String?
    var lat: String?
    var lng: String?
    
    // Mappable
    func mapping(map: Map) {
        address <- map["address"]
        lat  <- map["lat"]
        lng  <- map["lng"]
    }
}

