//
//  ShopItemPropertyDataModel.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 18/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import ObjectMapper

class ItemProperty: Mappable{
    
    var value: String?
    var label: String?
    var icon: String?
    
    // Mappable
    func mapping(map: Map) {
        value <- map["value"]
        label  <- map["label"]
        icon <- map["icon"]
    }
    
    required init?(map: Map) {
        // if map.JSON["sizes"] == nil {
        //     print("--------- error in json sizes ----------")
        //    return nil
        // }
    }
    
}
