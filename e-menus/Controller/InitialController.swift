//
//  ViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 18/03/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper


class InitialController: UIViewController {

    @IBOutlet weak var textField: UITextField?
    
    //Constants
    let WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather"
    // let APP_ID = "e72ca729af228beabd5d20e3b7749713"
    
    let APP_ID = "c53fdeccb8eddaf7ea8ce922cd6afb71"
    /***Get your own App ID at https://openweathermap.org/appid ****/
    
    let JSON_URL = "https://e-menus.optimedia.gr/app/sponsors"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // perform initial steps
        // HelperFunctions.shared.checkConnectivity()
        // HelperFunctions.shared.checkLocationServices()
        
        
        
    }

    
    let networkStatus = NetworkStatus.sharedInstance
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print(networkStatus.startNetworkReachabilityObserver(vc:self))
    }

    @IBAction func pressButton(){
        textField!.text = "Calling the http request to get sponsors";
        getSponsors( jsonUrl: JSON_URL);
        print("JSON called ansynchronously");
        
    }
    
    
    
    @IBAction func getShopsNearMe() {
        getShopsNearMe(jsonUrl: "https://e-menus.optimedia.gr/app/businesses?lat=37.978008&lon=23.707495&rng=2000&max=2")
    }
    
    @IBAction func getShopDetails() {
        getShopDetails(jsonUrl:"https://e-menus.optimedia.gr/app/business?id=45")
    }
    
    
    func getShopDetails ( jsonUrl: String) {
        
        Alamofire.request(jsonUrl).responseJSON { response in
            // print("Request: \(String(describing: response.request))")   // original url request
            // print("Response: \(String(describing: response.response))") // http url response
            // print("Result: \(response.result)")                         // response serialization result
            
            if response.result.isSuccess{
                // print ("JSON: \(String(describing: response.result.value))" );
                
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJsonStr);
                // self.jsonParsing(jsonStr: resultJsonStr);
                
                self.jsonParsingShopDetails(jsonStr: resultJsonStr)
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
    }
    
    
    func getShopsNearMe ( jsonUrl: String) {
        
        Alamofire.request(jsonUrl).responseJSON { response in
            // print("Request: \(String(describing: response.request))")   // original url request
            // print("Response: \(String(describing: response.response))") // http url response
            // print("Result: \(response.result)")                         // response serialization result
            
            if response.result.isSuccess{
                // print ("JSON: \(String(describing: response.result.value))" );
                
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJsonStr);
                // self.jsonParsing(jsonStr: resultJsonStr);
                
                self.jsonParsingShopsNearMe(jsonStr: resultJsonStr)
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
    }
   
    
    
    
    func getSponsors( jsonUrl: String) {
        
        Alamofire.request(jsonUrl).responseJSON { response in
            // print("Request: \(String(describing: response.request))")   // original url request
            // print("Response: \(String(describing: response.response))") // http url response
            // print("Result: \(response.result)")                         // response serialization result
            
            if response.result.isSuccess{
                // print ("JSON: \(String(describing: response.result.value))" );
                
                var resultJson: JSON = "{}"
                
                if (response.result.value != nil){
                    resultJson  = JSON(response.result.value as Any);
                }
                
                let resultJsonStr : String = String(describing: resultJson)
                
                print(resultJson);
                self.jsonParsing(jsonStr: resultJsonStr);
                
            } else {
                print("Return error message" + response.result.description);
            }
        }
        
    }
    
    
    
    //MARK: JSON parsing
    func jsonParsingShopsNearMe(jsonStr: String ){
        
        print("json str received" + jsonStr)
        let shopsNearMe : [ShopsNearMeDataModel] = Mapper<ShopsNearMeDataModel>().mapArray(JSONString: jsonStr)!
        
        // assign the read sponsors to the localstorage singleton
        // LocalStorageModel.sharedInstance.sponsors = sponsors
        
        // if (sponsors[0].img_landscape?.sizes != nil){
        //     print ("sponsor image url  name is" + sponsors[0].img_landscape!.sizes!.custom_0360! );
        // }
        
        // switchToNewViewController();
        print ("read --------")
        print(" read data mapable is " + String(shopsNearMe[0].translations![0].name!))
        
    }
    
    //MARK: JSON parsing
    func jsonParsingShopDetails(jsonStr: String ){
        
        print("json str received" + jsonStr)
        let shopDetails : [ShopDataModel] = Mapper<ShopDataModel>().mapArray(JSONString: jsonStr)!
        
        
        
        // assign the read sponsors to the localstorage singleton
        // LocalStorageModel.sharedInstance.sponsors = sponsors
        
        // if (sponsors[0].img_landscape?.sizes != nil){
        //     print ("sponsor image url  name is" + sponsors[0].img_landscape!.sizes!.custom_0360! );
        // }
        
        
        // switchToNewViewController();
        print ("read --------")
        print(" read data Shop Details is " + shopDetails[0].description!)
        
    }
    
    
    
    //MARK: JSON parsing
    func jsonParsing(jsonStr: String){
        
        print("json str received" + jsonStr)
        let sponsors : [SponsorDataModel] = Mapper<SponsorDataModel>().mapArray(JSONString: jsonStr)!
        
        if (sponsors[0].img_landscape?.filename != nil){
            print ("sponsor 0 image filename name is" + sponsors[0].img_landscape!.filename!);
        }
        
        if (sponsors[1].img_landscape?.filename != nil){
            print ("sponsor 1 image filename name is" + sponsors[1].img_landscape!.filename!);
        }
        
        if (sponsors[0].img_portrait?.sizes?.medium_large != nil) {
            print ("sponsor image 0=" + sponsors[0].img_portrait!.sizes!.medium_large!);
        }
        // assign the read sponsors to the localstorage singleton
        LocalStorageModel.sharedInstance.sponsors = sponsors
        
        // if (sponsors[0].img_landscape?.sizes != nil){
        //     print ("sponsor image url  name is" + sponsors[0].img_landscape!.sizes!.custom_0360! );
        // }
        
        
        switchToNewViewController();
        
    }
    
    @IBAction func switchToNewViewController(){
        // let secondViewController:SponsorViewController = SponsorViewController()
        
        // self.present(secondViewController, animated: true, completion: nil)
        
        performSegue(withIdentifier: "segue1", sender: self)

    }
}

