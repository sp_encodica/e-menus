//
//  ShopsNearMeViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 26/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import UIKit
import LazyImage
import Alamofire
import SideMenuSwift

class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var shopDescription: UILabel!
    @IBOutlet weak var infromations: UILabel!
    
    @IBOutlet weak var logoView: UIImageView!
}




class ShopsNearMeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var sponsorImageView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
   let ls = LocalStorageModel.sharedInstance
    
    lazy var lazyImage = LazyImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // get the singleton sponsors object
    
        self.navigationController!.setNavigationBarHidden( false, animated: true);
        
        self.navigationController!.navigationBar.barTintColor = UIColor.white
        self.navigationController!.navigationBar.tintColor = UIColor.black
        
        /*
        HelperFunctions.shared.getShopsNearMe(completion:{ (success) -> Void in
            self.displayShopsNearMe();
        }
 */
        self.displayShopsNearMe();
        
    }
    
    
    @IBAction func showMenu(_ sender: Any) {
        // performSegue(withIdentifier: "showMenu", sender: sender)
        print("trying to show menu")
        self.sideMenuController?.revealMenu()
    }
    
    func displayShopsNearMe(){
        print("shops are read")
        // switchToNewViewController();
        print ("read --------")
        
        
        print(" read data mapable is " + String(ls.shopsNearMe[0].translations![0].name!))
    }
    
    
    // UICOllection View delegate methods
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ls.shopsNearMe.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let reuseIdentifier = "cell"
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCell
        
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.myLabel.text = ls.shopsNearMe[indexPath.item].translations![0].name
        cell.shopDescription.text = ls.shopsNearMe[indexPath.item].translations![0].description
        // cell.backgroundColor = UIColor.cyan // make cell more visible in our example project
        
        
        
        let imageURL : String;
        
        if (ls.shopsNearMe[indexPath.item].translations![0].logo != nil){
            imageURL = ls.shopsNearMe[indexPath.item].translations![0].logo!
            self.lazyImage.showWithSpinner(imageView: cell.logoView, url: imageURL) {
                (error:LazyImageError?) in
                //Image loaded. Do something..
            }
        }
        
        
        let bottomLine = CALayer()
        let upLine = CALayer()
        
        bottomLine.frame = CGRect(x: 0.0, y: cell.frame.size.height - 1, width: cell.frame.size.width , height: 1.0)
        upLine.frame = CGRect(x: 0.0, y: 0, width: cell.frame.width , height: 1.0)
        
        let grey = UIColor(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 0.6)
        
        // let green = UIColor(red: 1.0/255.0, green: 255/255.0, blue: 0/255.0, alpha: 0.8)
        
        bottomLine.backgroundColor = grey.cgColor
        upLine.backgroundColor = grey.cgColor
        
        cell.layer.addSublayer(bottomLine)
        cell.layer.addSublayer(upLine)
        
    
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    // spread cell to full screen width
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let itemWidth = view.bounds.width
            let itemHeight = layout.itemSize.height
            layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
            layout.invalidateLayout()
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        print("You selected cell #\(indexPath.item)!")
        
        // call the appropriate controller
        if let shopDetailsViewController = self.storyboard!.instantiateViewController(withIdentifier: "ShopDetails") as? ShopDetailsViewController {
            // let navigationController = UINavigationController(rootViewController: vc)
            // push new controller to navigationCOntroller in order to have Navigatin bar
            // provide the business ID
            shopDetailsViewController.businessId = 391;
            self.navigationController?.pushViewController(shopDetailsViewController, animated: true)
            
        }
        
    }
    
}
