//
//  LaunchViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 30/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {

    @IBOutlet var launchView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //addGradientToView(view: launchView)
        
    }
    
    func addGradientToView(view: UIView)
    {
        //gradient layer
        let gradientLayer = CAGradientLayer()
        
        //define colors
        gradientLayer.colors = [UIColor.init(red: 167/255, green: 24/255, blue: 28/255, alpha: 1.0),    UIColor.init(red: 226/255, green: 1/255, blue: 43/255, alpha: 1.0)]
        
        //define locations of colors as NSNumbers in range from 0.0 to 1.0
        //if locations not provided the colors will spread evenly
        //gradientLayer.locations = [0.0, 0.6, 0.8]
        
        //define frame
        gradientLayer.frame = view.bounds
        
        //insert the gradient layer to the view layer
        view.layer.insertSublayer(gradientLayer, at: 0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
