//
//  MainMenuController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 18/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON
import ObjectMapper
import SideMenuSwift


class MainMenuController: UIViewController{
    
    let menu : [String] = ["Menu1", "menu2", "Menu3"]
    
    
    
    @IBOutlet weak var testLabel: UILabel!
    
    
    @IBOutlet weak var menuTable: UITableView! {
        didSet {
            menuTable.dataSource = self
            menuTable.delegate = self
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        print("Main Menu controller")
        // [self performSegueWithIdentifier:@"DisplayAlternateView" sender:self];
        // sideMenuController?.delegate = self
        
        // Initialize sequences
        initialize()
        
        testLabel.text = "Hello"
        
        // sideMenuController?.delegate = self
        menuTable.backgroundColor = UIColor.init(red: 200, green: 0, blue: 0, alpha: 0.8)
        
       
        
    }
    
    
    func initialize() {
       
        // Set preferences fro SideMenu Operation
        print ("Initializing Sidemenu")
        SideMenuController.preferences.basic.position = SideMenuController.Preferences.MenuPosition.under
        SideMenuController.preferences.basic.menuWidth = 340
        SideMenuController.preferences.basic.defaultCacheKey = "0"
        // SideMenuController.preferences.basic.direction = .left
    }

    
    // Helper functions
   
    
   
}
//MARK TABLEVIEW JANDLING
extension MainMenuController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        print ("number of sections")
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menu.count
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SelectionCell
        
        // cell.contentView.backgroundColor = themeColor
        // let row = indexPath.row
        // if row == 0 {
        cell.titleLabel?.text = menu[indexPath.row]
            
        
        // cell.textLabel?.text = menu[indexPath.row]
        print("cell" + menu[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let row = indexPath.row
        
        // sideMenuController?.setContentViewController(with: "\(row)", animated: Preferences.shared.enableTransitionAnimation)
        // sideMenuController?.hideMenu()
        
        if let identifier = sideMenuController?.currentCacheIdentifier() {
            print("[Example] View Controller Cache Identifier: \(identifier)")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
}

class SelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}
