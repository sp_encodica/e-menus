//
//  CatalogueViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 25/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import Foundation
import UIKit
import SideMenuSwift
import DrawerView



class CatalogueViewController: UIViewController {
    
    
    // connection to UItableView
    @IBOutlet weak var catalogueTable: UITableView! {
        didSet {
            catalogueTable.dataSource = self
            catalogueTable.delegate = self
        }
    }
    
    @IBAction func openDrawer(_ sender: Any) {
        print("Open Drawer command")
        findParentDrawerView(ofView: self.view)?.setPosition(.open, animated: true)
    }
    
    //local variables
    // access the singleton
    var ls = LocalStorageModel.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // [self performSegueWithIdentifier:@"DisplayAlternateView" sender:self];
        
         print("Catalog controller")
        
        // reference to the internal navigation controller
         self.navigationController?.navigationBar.barTintColor = UIColor.white
         self.navigationController?.navigationBar.tintColor = UIColor.black
    }
   
    /*
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 10
    }
    
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    */
    
   
    @IBAction func pressButton(_ sender: Any) {
        print ("press button")
        sideMenuController?.revealMenu()
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        print ("press button")
        sideMenuController?.revealMenu()
    }
    
    
    // Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
    // Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        cell.textLabel?.text = "hello" // menu[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showCatalog", sender: self)
    }
    */
    
    func findParentDrawerView(ofView view: UIView?) -> DrawerView? {
        switch view?.superview {
        case .none:
            return nil
        case .some(let parent):
            return parent as? DrawerView ?? findParentDrawerView(ofView: view)
        }
    }
}

//MARK TABLEVIEW JANDLING
extension CatalogueViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        var sectionName: String = ""
        
        if (ls.shopDetails[0].menus?[0].menu_categories != nil){
            sectionName = ls.shopDetails[0].menus![0].menu_categories![section].category_name!
        }
        
        return sectionName
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        var numOfCategories : Int = 0;
        
        print (" test print ->", String(ls.shopDetails[0].name!))
        
        if (ls.shopDetails[0].menus?[0].menu_categories != nil){
            numOfCategories = ls.shopDetails[0].menus![0].menu_categories!.count
            
            
        }
        print ("number of categories", numOfCategories)
        return numOfCategories
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numOfRowsInSection : Int = 0
        
        if (ls.shopDetails[0].menus?[0].menu_categories?[section].category_items?.count != nil) {
            numOfRowsInSection = (ls.shopDetails[0].menus?[0].menu_categories![section].category_items!.count)!
        }
        return numOfRowsInSection
    }
    
    // swiftlint:disable force_cast
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "catalogueItemSelectionCell", for: indexPath) as! catalogueItemSelectionCell
        
        // cell.contentView.backgroundColor = themeColor
        // let row = indexPath.row
        // if row == 0 {
        // cell.titleLabel?.text = menu[indexPath.row]
   
        /*
        if (ls.shopDetails[0].menus?[0].menu_categories?[indexPath.section] != nil) {
            print ("OK")
            
            if (ls.shopDetails[0].menus?[0].menu_categories?[indexPath.section].category_items?.count != nil) {
                print ("2nd OK", ls.shopDetails[0].menus![0].menu_categories![indexPath.section].category_items!.count, " ", indexPath.row )
                // print (ls.shopDetails[0].menus![0].menu_categories![indexPath.section].category_items![indexPath.row].item_name!)
            }
            
        }
 */
        
        
        if (ls.shopDetails[0].menus?[0].menu_categories?[indexPath.section].category_items?[indexPath.row] != nil){
            cell.titleLabel?.text =  ls.shopDetails[0].menus![0].menu_categories![indexPath.section].category_items![indexPath.row].item_name!
        }
 
        // cell.textLabel?.text = menu[indexPath.row]
        // print("cell" + menu[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // let row = indexPath.row
        
        // sideMenuController?.setContentViewController(with: "\(row)", animated: Preferences.shared.enableTransitionAnimation)
        // sideMenuController?.hideMenu()
        
        if let identifier = sideMenuController?.currentCacheIdentifier() {
            print("[Example] View Controller Cache Identifier: \(identifier)")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    
}

class catalogueItemSelectionCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}


