//
//  ShopDetailsViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 29/04/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import UIKit
import ImageSlideshow
import AlamofireImage
import DrawerView


class ShopDetailsViewController: UIViewController {

    @IBOutlet weak var slideshow : ImageSlideshow!
    var businessId : Int = 0
    
    let ls = LocalStorageModel.sharedInstance
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print ("Business ID is " , businessId)
        
        
        // attach drawer controller
        
       
        getBusinessDetails()
        
    }
    
    
    func setUpDrawer() {
        let drawerViewController = self.storyboard!.instantiateViewController(withIdentifier: "ShopCatalogDrawer")
        let drawerView : DrawerView = addDrawerView(withViewController: drawerViewController)
        
        drawerView.position = .collapsed
        drawerView.snapPositions = [.collapsed, .open]
        drawerView.position = .collapsed

    }
    
    func getBusinessDetails(){
        
        HelperFunctions.shared.getShopDetails(businessId: self.businessId, completion:{ (success) -> Void in
            // we got shop details from AlamoFire
            
            print(String(self.ls.shopDetails[0].name!))
            
             self.setupSlideShow()
            
             self.setUpDrawer()
            
        }
        )
    }
    
    func setupSlideShow(){
        
        // setup slideshow viewer
        let pageIndicator = UIPageControl()
        pageIndicator.currentPageIndicatorTintColor = UIColor.lightGray
        pageIndicator.pageIndicatorTintColor = UIColor.black
        slideshow.pageIndicator = pageIndicator
        
        var imageInputs : [AlamofireSource] = []
        
        // add logo
        if (ls.shopDetails[0].media?.logo?.url != nil){
            imageInputs.append(AlamofireSource(urlString: ls.shopDetails[0].media!.logo!.url!)!)
        }
        
        // add additional Images
        if (ls.shopDetails[0].media?.image_gallery != nil){
            // print ("there is image gallery")
            
            for slideShowImage in ls.shopDetails[0].media!.image_gallery! {
                imageInputs.append(AlamofireSource(urlString: slideShowImage.url!)!)
                // print (String(slideShowImage.filename!))
            }
        }
       
      
        slideshow.setImageInputs(imageInputs)
       
        
        /*
        slideshow.setImageInputs([
            AlamofireSource(urlString: "https://images.unsplash.com/photo-1432679963831-2dab49187847?w=1080")!
            ])
         */
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
