//
//  SponsorViewController.swift
//  e-menus
//
//  Created by Stefanos Papachristou on 22/03/2019.
//  Copyright © 2019 Solutions Encodica. All rights reserved.
//

import UIKit
import LazyImage
import Alamofire
import SideMenuSwift


class SponsorViewController: UIViewController {
    
    @IBAction func showMenu(_ sender: Any) {
        self.sideMenuController?.revealMenu()
    }
    
    
    @IBOutlet weak var sponsorImageView: UIImageView!
    
    lazy var lazyImage = LazyImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // get the singleton sponsors object
        
        // let ls = LocalStorageModel.sharedInstance
        print("calling sponsors")
        
        //remove navigation bar
        // self.navigationController!.setNavigationBarHidden( true, animated: true);
        
        let  navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = uicolorFromHex(rgbValue: 0xffffff)
        navigationBarAppearace.barTintColor = uicolorFromHex(rgbValue: 0xffffff)
       
        self.navigationItem.title = ""
        
       /*
        method(arg: true, completion: { (success) -> Void in
            print("Second line of code executed")
            if success { // this will be equal to whatever value is set in this method call
                print("true")
            } else {
                print("false")
            }
        })
        */
        
        HelperFunctions.shared.getSponsors(completion:{ (success) -> Void in
            self.displaySponsorImage();
            }
        )
        
    }
    
    
    func uicolorFromHex(rgbValue:UInt32)->UIColor{
        let red = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue = CGFloat(rgbValue & 0xFF)/256.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func displaySponsorImage(){
        
        let ls = LocalStorageModel.sharedInstance
        
        var imageURL = ""
        
        if (ls.sponsors[0].img_portrait?.sizes?.medium_large != nil) {
            print ("sponsor image 0=" + ls.sponsors[0].img_portrait!.sizes!.medium_large!);
            // imageURL = ls.sponsors[0].img_portrait!.sizes!.medium_large!
            imageURL = ls.sponsors[0].img_portrait!.sizes!.medium_large!
            self.lazyImage.showWithSpinner(imageView: self.sponsorImageView, url: imageURL) {
                (error:LazyImageError?) in
                //Image loaded. Do something..
                self.startTimer()
            }
            
        }
        
    }
    
    func startTimer() {
        _ = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false, block: { timer in
            print("FIRE!!!")
            // move to the next comtroller
            
            // prepare data and call the next controller
            
            HelperFunctions.shared.getShopsNearMe(completion:{ (success) -> Void in
                // self.displayShopsNearMe();
                if let shopsNearMeViewController = self.storyboard!.instantiateViewController(withIdentifier: "ShopsNearMe") as? ShopsNearMeController {
                    
                     // let navigationController = UINavigationController(rootViewController: vc)
                    // push new controller to navigationCOntroller in order to have Navigatin bar
                    self.navigationController?.pushViewController(shopsNearMeViewController, animated: true)
                   
                }
            }
            )
            
           
        })
        
    }
}

